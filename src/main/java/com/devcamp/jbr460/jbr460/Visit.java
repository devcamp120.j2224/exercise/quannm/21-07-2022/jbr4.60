package com.devcamp.jbr460.jbr460;

import java.util.Date;

public class Visit {
    private Customer customer;
    private Date date;
    private double serviceExpense;
    private double productExpense;

    public Visit(Customer customer, Date date) {
        this.customer = customer;
        this.date = date;
    }

    public String getName() {
        return this.customer.getName();
    }

    public double getServiceExpense() {
        return serviceExpense;
    }

    public void setServiceExpense(double serviceExpense) {
        this.serviceExpense = serviceExpense;
    }

    public double getProductExpense() {
        return productExpense;
    }

    public void setProductExpense(double productExpense) {
        this.productExpense = productExpense;
    }

    public double getTotalExpense() {
        return getProductExpense() + getServiceExpense();
    }

    @Override
    public String toString() {
        return "Visit[Customer[name=" + customer.getName() 
                        + ",member=" + customer.isMember() 
                        + ",memberType=" + customer.getMemberType() + "]" 
                        + ",date=" + this.date + ",serviceExpense=" 
                        + this.serviceExpense + ",productExpense=" 
                        + this.productExpense + "]";
    }
}
