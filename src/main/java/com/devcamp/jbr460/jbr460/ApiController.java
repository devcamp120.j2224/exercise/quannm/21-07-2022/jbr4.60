package com.devcamp.jbr460.jbr460;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ApiController {
    @CrossOrigin
    @GetMapping("/visits")
    public ArrayList<Visit> getListVisit() {
        ArrayList<Visit> listVisit = new ArrayList<Visit>();

        Customer customer1 = new Customer("QuanNM");
        Customer customer2 = new Customer("BoiHB");
        Customer customer3 = new Customer("ThuNHM");

        Visit visit1 = new Visit(customer1, new Date());
        Visit visit2 = new Visit(customer2, new Date());
        Visit visit3 = new Visit(customer3, new Date());

        listVisit.add(visit1);
        listVisit.add(visit2);
        listVisit.add(visit3);

        return listVisit;
    }

    // public static void main(String[] args) {
    //     Customer customer1 = new Customer("QuanNM");
    //     Customer customer2 = new Customer("BoiHB");
    //     Customer customer3 = new Customer("ThuNHM");

    //     System.out.println(customer1 + "," + customer2 + "," + customer3);

    //     Visit visit1 = new Visit(customer1, new Date());
    //     Visit visit2 = new Visit(customer2, new Date());
    //     Visit visit3 = new Visit(customer3, new Date());

    //     System.out.println(visit1 + "," + visit2 + "," + visit3);
    // }
}
