package com.devcamp.jbr460.jbr460;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Jbr460Application {

	public static void main(String[] args) {
		SpringApplication.run(Jbr460Application.class, args);
	}

}
